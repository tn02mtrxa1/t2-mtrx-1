//EXPONENTIAL
double pow(double x, int n){
		
	float value = 1;
	if(x==0){
		value = 0;
		printf("The value calculated : %.3f.\n\n\n", value);
	}else if(x!=0){

		while(n != 0){

			value *= x;
			--n;
		}	
		printf("The value calculated : %.3f.\n\n\n", value);
	}
	return 0; 

//G.C.D.
}

int gcd(int a, int b){
	int gCD, rem;
	    for(rem=1; rem <= a && rem <= b; ++rem)
		{
			if(a%rem==0 && b%rem==0)
		        gCD = rem;
		}
		printf("G.C.D of %d and %d is %d.\n\n\n", a, b, gCD);
	return 0;
}


//PRIME
int is_prime(unsigned int x){
	int num = 0 , i ;
	if(x==1 || x==0){
		printf("The no. entered is not a prime no.\n\n\n");
	}else{
		for(i=1 ; i<=x ; ++i)
		{
			if(x%i == 0)
			{
				num++;
			}		
		}
		
		if(num == 2){
			printf("1\n\n\n");
		}else if(num > 2){
			printf("0\n\n\n");
		}
	}
	return 0;
}


//FUTURE VALUE
double FV(double rate, unsigned int nperiods, double PV)
{
	int i ;
	double fV, num =1;

	rate = rate + 1;
	if(rate<=0 || nperiods<=0 || PV<=0){
		printf("Data entered is incorrect.\n\n\n");
	}else{
		for(i=1; i<=nperiods; i++){

			num *= rate;

		}	
		fV = PV * num;
		printf("\nThe Future value for invested money Rs. %0.3lf at a rate of interest of %0.3lf%% for a period of %u years is %0.2lf. \n\n\n", PV, rate, nperiods, fV);
	}
	return 0;
}


//PRESENT VALUE
double PV(double rate, unsigned int nperiods, double FV)
{
	int i ;
	double pV, num =1;

	rate = rate + 1;
	if(rate<=0 || nperiods<=0 || FV<=0){
		printf("Data entered is incorrect.\n\n\n");
	}else{
		for(i=1; i<=nperiods; i++){

			num *= rate;

		}	
		pV = FV / num;
		printf("\nThe Present value for Future money value Rs. %0.3lf at a rate of interest of %0.3lf%% for a period of %u years is %0.2lf. \n\n\n", FV, rate, nperiods, pV);
	}
	return 0;
}


//FACTOR
void factor( int value)
{
	int i;
	if(value <=0 ){
		printf("Data is incorrect");
	}else{
		for (i=1; i<= value; ++i){
			if(value%i == 0){
				printf("The no %d is a factor of %d.\n", i, value);

			}
		}
	}
	printf("\n\n");
	return ;
}


//PRIME FACTOR
void pfactor( int value)
{
	int i, p, no;
	if(value <=0 ){
		printf("Data is incorrect");
	}else if(value > 0){
		for (i=2; i<= value; ++i){
			if(value%i == 0){
				no =1;
				for(p=2; p<=i/2; p++){
					if(i%p==0){
						no = 0;
						break;
					}
				}
				if(no ==1){

					printf("The value %d is prime Factor of %d.\n", i, value);

				}	
			}
		}
	}
	printf("\n\n");
	return ;
}


//EVEN ODD
void even_odd(int value)
{
	while(value >=0){
		value = value%2;
		if(value == 1){

 			printf("The entered no is odd number.\n\n\n");
			break;

		}else{
			printf("The entered no is even number.\n\n\n");
			break;
		}
		

	}
	return ;
}


//L.C.M
void lCM(int a,int b, int c)
{
	int p,add,lcm ;
	if(a<b && b<c){
		p = add = c;
	}else if(a<b && c<b){
		p = add = b;
	}else{
		p = add = a;
	}
	
	while(1){
		if(p%a==0 && p%b==0 && p%c==0){
			lcm=p;
			break;
		}
	   p += add;
	}
	
	printf("The L.C.M of the entered three nos. is %d.\n\n\n", lcm);
	return;
}


//FACTORIAL
void factorial(int num)
{
	int i;
	long int fact=1;
	if(num < 0){
		printf("Enter a valid value to perform factorial! ");
	}else if(num == 0){
		printf("The Factorial for the given value is %ld.\n\n\n", fact);
	}else{
		for(i=1; i<=num; i++){

			fact = fact*i;

		}
	        printf("The Factorial for the given value is %ld.\n\n\n", fact);
	}
        return ;
}


//nCr
void nCr(int N,int R)
{
	int i;
	long int v1=1,v2=1,v3=1,NCR=1;
	if(N<= 0 || R<=0 || N<R){
		printf("Enter a valid value to perform nCr! \n\n\n");
	}else{
		for(i=1; i<=N; i++){

			v1 = v1*i;

		}
		for(i=1; i<=R; i++){

			v2 = v2*i;

		}
		for(i=1; i<=N-R; i++){

			v3 = v3*i;

		}
		NCR= v1/(v2*v3);
	        printf("The nCR is %ld.\n\n\n", NCR);
	}
        return ;
}


//nPr
void nPr(int N,int R)
{
	int i;
	long int v1=1,v2=1,NPR=1;
	if(N<= 0 || R<=0 || N<R){
		printf("Enter a valid value to perform nPr! \n\n\n");
	}else{
		for(i=1; i<=N; i++){

			v1 = v1*i;

		}
		for(i=1; i<=N-R; i++){

			v2 = v2*i;

		}
		NPR= v1/(v2);
	        printf("The nPR is %ld.\n\n\n", NPR);
	}
        return ;
}


//FIBO
void fibo(int dig)
{

	int i ,num, n1=0, n2=1;

	printf("\nThe 0 digit of fibonacci series is : %d\n",n1);
	printf("The 1 digit of fibonacci series is : %d\n",n2);

	for(i=2; i<=dig; i++){
		num=n1+n2;

		printf("The %d digit of fibonacci series is : %d\n", i, num);

		n1=n2;
		n2=num;
	}
	return;

}









